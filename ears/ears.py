from pocketsphinx import LiveSpeech 
from mouth import Talk

class Listen:
    def __init__(self):
        self.speech = LiveSpeech(lm=False, keyphrase='atlas', kws_threshold=1e-20)
        self.perkup()

    def perkup(self):
        for phrase in self.speech:
            Talk("how can I be of service, sir?")

if __name__ == '__main__':
    listen = Listen()
