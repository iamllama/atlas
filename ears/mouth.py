import pyttsx3

class Talk:
    def __init__(self, args):
        self.engine = pyttsx3.init()
        self.arg = args
        self.sayit(self.arg)
    
    def sayit(self, whattosay):
        self.engine.say(whattosay)
        self.engine.runAndWait()
