#    Created @ - 1:53PM
#    Created By - llama (Sahil)
#    Created On - VSCode
#    Created Under - llamasec [owned by Sahil (llama)]
#    Lisence - GNU General Public License v3 (GPL-3)
#!/bin/python3
from re import compile
from sys import stderr
import requests as r
from bs4 import BeautifulSoup as bs

# https://api.duckduckgo.com/?q=valley+forge+national+park&format=json&pretty=1
# https://en.wikipedia.org/wiki/

"""
    Summary is a simple module in a larger sceheme.
    It get's any* topic summary and return the essentials, like,
    what, who, when, where, (hopefully how), follow up links.
"""


class GetSummary:
    """
        Initializes the class variables, including the wikipedia and duckduckgo url.
        calls the get_summary_text() function to print the first "About" Paragraph
        inside the Wiki page, if it's found.

        Get's the raw html data from wikipedia,
        converts that into `BeautifulSoup` object
        removes all the HTML tags,
        prints the output
        On error? it prints out the 404 status code
    """

    def __init__(self, query):
        self.query = query
        self.wiki_url = f"https://en.wikipedia.org/wiki/{self.query}"
        self.ducky_url = f"https://api.duckduckgo.com/?q={self.query}&format=json&pretty=1"
        self.getwikisummary()

    def getwikisummary(self):
        get_data = r.get(self.wiki_url)
        if get_data.status_code == 200:
            soup = bs(get_data.text, 'lxml')
            output_clener = compile(r'<[^>]+>')
            summary = str(soup.find_all('p')[1])
            print(output_clener.sub('', summary))
        else:
            print("Calling dax to the rescue", stderr)
            self.getduckysummary()
    
    def getduckysummary(self):
        get_data = r.get(self.ducky_url)
        if get_data.status_code == 200:
            summary = get_data.json()
            print(summary['RelatedTopics'][0]['Text'])
