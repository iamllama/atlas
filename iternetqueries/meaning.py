#    Created @ - 9:00PM
#    Created By - llama (Sahil)
#    Created On - VSCode
#    Created Under - llamasec [owned by Sahil (llama)]
#    Lisence - GNU General Public License v3 (GPL-3)
#!/bin/python3
from re import compile
from sys import stderr
import requests as r
from bs4 import BeautifulSoup as bs
from spellchecker import SpellChecker
from PyDictionary import PyDictionary


class GetMeaning:
    def __init__(self, query):
        self.spellcheck = SpellChecker()
        self.query = query
        self.dict = PyDictionary()
        self.online_dict_url = f"https://www.thefreedictionary.com/{query}"
        self.get_offline_meaning(self.query)

    def check_spelling(self, word):
        # find those words that may be misspelled
        misspelled = self.spellcheck.unknown(word)
        for word in misspelled:
            # Get the one `most likely` answer
            if self.spellcheck.correction(word) == None:
                return word
            else:
                print(self.spellcheck.correction(word))

    def get_offline_meaning(self, word):
        print(self.dict.meaning(word)['Noun'])

    def get_online_meaning(self):
        get_data = r.get(self.online_dict_url)
        if get_data.status_code == 404:
            print(stderr, 'Coudnt find the meaning ')
        soup = bs(get_data.text, 'lxml')
        waldo = soup.find('div', {'class': 'ds-list'})
        return str(waldo)

    def clean_output(self):
        output_clener = compile(r'<[^>]+>')
        cleanoutput = self.get_online_meaning()
        print(output_clener.sub('', cleanoutput))

if __name__ == '__main__':
    while 1:
        userinput = input("enter ")
        a = GetMeaning(userinput)
